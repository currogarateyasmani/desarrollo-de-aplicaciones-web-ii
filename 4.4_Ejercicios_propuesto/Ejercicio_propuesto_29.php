<!DOCTYPE html>
<html>
<head>
    <title>Ciudad de destino según puntaje y sexo</title>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="sexo">Sexo:</label>
        <select name="sexo" id="sexo">
            <option value="masculino">Masculino</option>
            <option value="femenino">Femenino</option>
        </select><br>

        <label for="puntaje">Puntaje del examen:</label>
        <input type="number" name="puntaje" id="puntaje"><br>

        <input type="submit" value="Determinar ciudad">
    </form>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $sexo = $_POST['sexo'];
            $puntaje = $_POST['puntaje'];
            $ciudad = '';

            if ($sexo == 'masculino') {
                if ($puntaje >= 18 && $puntaje <= 35) {
                    $ciudad = 'Arequipa';
                } else if ($puntaje >= 36 && $puntaje <= 75) {
                    $ciudad = 'Cuzco';
                } else if ($puntaje > 75) {
                    $ciudad = 'Iquitos';
                }
            } else if ($sexo == 'femenino') {
                if ($puntaje >= 18 && $puntaje <= 35) {
                    $ciudad = 'Cuzco';
                } else if ($puntaje >= 36 && $puntaje <= 75) {
                    $ciudad = 'Iquitos';
                } else if ($puntaje > 75) {
                    $ciudad = 'Arequipa';
                }
            }

            echo "La ciudad de destino es: $ciudad";
        }
    ?>
</body>
</html>
