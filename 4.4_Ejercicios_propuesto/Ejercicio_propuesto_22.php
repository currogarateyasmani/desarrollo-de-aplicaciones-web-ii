<!DOCTYPE html>
<html>
<head>
    <title>Día de la semana</title>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="dia">Ingresa un número del 1 al 7:</label>
        <input type="number" name="dia" id="dia"><br>

        <input type="submit" value="Comprobar">
    </form>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $dia = $_POST['dia'];

            switch ($dia) {
                case 1:
                    echo "Domingo";
                    break;
                case 2:
                    echo "Lunes";
                    break;
                case 3:
                    echo "Martes";
                    break;
                case 4:
                    echo "Miércoles";
                    break;
                case 5:
                    echo "Jueves";
                    break;
                case 6:
                    echo "Viernes";
                    break;
                case 7:
                    echo "Sábado";
                    break;
                default:
                    echo "Ingresa un número del 1 al 7";
                    break;
            }
        }
    ?>
</body>
</html>
