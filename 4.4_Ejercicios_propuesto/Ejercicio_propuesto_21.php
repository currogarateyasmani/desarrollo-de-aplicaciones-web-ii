<!DOCTYPE html>
<html>
<head>
    <title>Mes en letras</title>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="numero">Número del mes:</label>
        <input type="number" name="numero" id="numero"><br>

        <input type="submit" value="Obtener">
    </form>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $numero = $_POST['numero'];

            switch ($numero) {
                case 1:
                    echo "Enero";
                    break;
                case 2:
                    echo "Febrero";
                    break;
                case 3:
                    echo "Marzo";
                    break;
                case 4:
                    echo "Abril";
                    break;
                case 5:
                    echo "Mayo";
                    break;
                case 6:
                    echo "Junio";
                    break;
                case 7:
                    echo "Julio";
                    break;
                case 8:
                    echo "Agosto";
                    break;
                case 9:
                    echo "Septiembre";
                    break;
                case 10:
                    echo "Octubre";
                    break;
                case 11:
                    echo "Noviembre";
                    break;
                case 12:
                    echo "Diciembre";
                    break;
                default:
                    echo "Número de mes inválido";
            }
        }
    ?>
</body>
</html>
