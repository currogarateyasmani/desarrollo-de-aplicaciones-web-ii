<!DOCTYPE html>
<html>
<head>
    <title>Estado Civil</title>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="codigo">Código:</label>
        <input type="number" name="codigo" id="codigo"><br>

        <input type="submit" value="Obtener">
    </form>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $codigo = $_POST['codigo'];

            switch ($codigo) {
                case 0:
                    echo "Soltero";
                    break;
                case 1:
                    echo "Casado";
                    break;
                case 2:
                    echo "Divorciado";
                    break;
                case 3:
                    echo "Viudo";
                    break;
                default:
                    echo "Código inválido";
                    break;
            }
        }
    ?>
</body>
</html>
