<!DOCTYPE html>
<html>
<head>
    <title>Nombre de canal de televisión</title>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="canal">Número de canal:</label>
        <input type="number" name="canal" id="canal"><br>

        <input type="submit" value="Buscar">
    </form>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $canales = array(
                2 => "Canal 2",
                4 => "Canal 4",
                6 => "Canal 6",
                8 => "Canal 8",
                11 => "Canal 11",
                13 => "Canal 13"
            );

            $canal = $_POST['canal'];

            if (array_key_exists($canal, $canales)) {
                echo "El canal $canal es " . $canales[$canal];
            } else {
                echo "No se pudo encontrar ese canal con el número $canal.";
            }
        }
    ?>
</body>
</html>
