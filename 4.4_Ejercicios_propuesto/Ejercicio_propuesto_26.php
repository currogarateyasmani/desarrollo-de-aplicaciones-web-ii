<!DOCTYPE html>
<html>
<head>
    <title>Calculadora de descuentos en manzanas</title>
</head>
<body>
    <h2>Calculadora de descuentos en manzanas</h2>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="kilos">Cantidad de kilos:</label>
        <input type="number" name="kilos" id="kilos"><br><br>
        <input type="submit" value="Calcular precio">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $kilos = $_POST['kilos'];
        $precio = 0;

        if ($kilos <= 2) {
            $precio = $kilos * 10;
        } elseif ($kilos <= 5) {
            $precio = $kilos * 9;
        } elseif ($kilos <= 10) {
            $precio = $kilos * 8;
        } else {
            $precio = $kilos * 7;
        }

        echo "<p>Usted compró $kilos kilos de manzanas.</p>";
        echo "<p>El precio total de su compra es: $$precio</p>";
    }
    ?>
</body>
</html>
