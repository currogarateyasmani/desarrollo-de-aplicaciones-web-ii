<!DOCTYPE html>
<html>
<head>
    <title>Días faltantes para que acabe el año</title>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="fecha">Fecha (YYYY-MM-DD):</label>
        <input type="text" name="fecha" id="fecha"><br>

        <input type="submit" value="Calcular">
    </form>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $fecha = $_POST['fecha'];
            $dias_faltantes = intval(date('z', strtotime('31-12-' . date('Y'))) - date('z', strtotime($fecha))) + 1;

            echo "Faltan $dias_faltantes días para que acabe el año.";
        }
    ?>
</body>
</html>
