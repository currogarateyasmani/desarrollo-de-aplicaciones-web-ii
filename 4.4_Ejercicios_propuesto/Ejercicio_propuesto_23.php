<!DOCTYPE html>
<html>
<head>
    <title>Nombre de operador aritmético</title>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="operador">Ingrese un operador aritmético (+, -, * o /):</label>
        <input type="text" name="operador" id="operadoracion"><br>

        <input type="submit" value="Enviar">
    </form>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $operador = $_POST['operador'];

            switch ($operador) {
                case '+':
                    echo "La operacion suma (+)";
                    break;
                case '-':
                    echo "El operador es resta (-)";
                    break;
                case '*':
                    echo "la operacion multiplicación (*)";
                    break;
                case '/':
                    echo "El operador división (/)";
                    break;
                default:
                    echo "El operador ingresado no es válido";
                    break;
            }
        }
    ?>
</body>
</html>
