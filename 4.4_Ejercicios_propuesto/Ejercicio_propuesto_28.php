<!DOCTYPE html>
<html>
<head>
    <title>Calculadora de utilidades para trabajadores</title>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="tiempo_servicio">Tiempo de servicio (en años):</label>
        <input type="number" name="tiempo_servicio" id="tiempo_servicio"><br>

        <label for="cargo">Cargo:</label>
        <select name="cargo" id="cargo">
            <option value="administrador">Administrador</option>
            <option value="contador">Contador</option>
            <option value="empleado">Empleado</option>
        </select><br>

        <input type="submit" value="Calcular">
    </form>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $tiempo_servicio = $_POST['tiempo_servicio'];
            $cargo = $_POST['cargo'];

            $monto_utilidades = 0;

            if ($tiempo_servicio >= 0 && $tiempo_servicio <= 2) {
                switch ($cargo) {
                    case 'administrador':
                        $monto_utilidades = 2000;
                        break;
                    case 'contador':
                        $monto_utilidades = 1500;
                        break;
                    case 'empleado':
                        $monto_utilidades = 1000;
                        break;
                    default:
                        echo "Error: cargo no válido";
                        exit();
                }
            } elseif ($tiempo_servicio >= 3 && $tiempo_servicio <= 5) {
                switch ($cargo) {
                    case 'administrador':
                        $monto_utilidades = 2500;
                        break;
                    case 'contador':
                        $monto_utilidades = 2000;
                        break;
                    case 'empleado':
                        $monto_utilidades = 1500;
                        break;
                    default:
                        echo "Error: cargo no válido";
                        exit();
                }
            } elseif ($tiempo_servicio >= 6 && $tiempo_servicio <= 8) {
                switch ($cargo) {
                    case 'administrador':
                        $monto_utilidades = 3000;
                        break;
                    case 'contador':
                        $monto_utilidades = 2500;
                        break;
                    case 'empleado':
                        $monto_utilidades = 2000;
                        break;
                    default:
                        echo "Error: cargo no válido";
                        exit();
                }
            } elseif ($tiempo_servicio > 8) {
                switch ($cargo) {
                    case 'administrador':
                        $monto_utilidades = 4000;
                        break;
                    case 'contador':
                        $monto_utilidades = 3500;
                        break;
                    case 'empleado':
                        $monto_utilidades = 1500;
                        break;
                    default:
                        echo "Error: cargo no válido";
                        exit();
                }
            } else {
                echo "Error: tiempo de servicio no válido";
                exit();
            }

            echo "El monto de utilidades para un trabajador con cargo de $cargo y $tiempo_servicio años de servicio es de: $monto_utilidades";
        }
    ?>
</body>
</html>
