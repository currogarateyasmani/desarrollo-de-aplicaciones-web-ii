<!DOCTYPE html>
<html>
<head>
    <title>Calcular el descuento salarial</title>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="sexo">Sexo:</label>
        <input type="radio" name="sexo" value="hombre"> Hombre
        <input type="radio" name="sexo" value="mujer"> Mujer<br>

        <label for="tarjeta">Tipo de tarjeta:</label>
        <input type="radio" name="tarjeta" value="obrero"> Obrero
        <input type="radio" name="tarjeta" value="empleado"> Empleado<br>

        <label for="sueldo">Sueldo:</label>
        <input type="number" name="sueldo" id="sueldo"><br>

        <input type="submit" value="Calcular descuento">
    </form>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $sexo = $_POST['sexo'];
            $tarjeta = $_POST['tarjeta'];
            $sueldo = $_POST['sueldo'];
            $descuento = 0;

            if ($sexo == 'hombre' && $tarjeta == 'obrero') {
                $descuento = $sueldo * 0.15;
            } elseif ($sexo == 'hombre' || $sexo == 'mujer') {
                $descuento = $sueldo * 0.2;
            }

            if ($tarjeta == 'obrero') {
                $descuento = $sueldo * 0.1;
            }

            echo "El descuento aplicado es de $" . $descuento;
        }
    ?>
</body>
</html>
