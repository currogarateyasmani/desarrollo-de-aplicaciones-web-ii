<?php
function invertir_numero($numero) {
  $numero_invertido = strrev($numero);
  return $numero_invertido;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $numero = $_POST['numero'];
  $numero_invertido = invertir_numero($numero);
  echo "El número invertido de $numero es: $numero_invertido";
}
?>

<form method="post">
  <label for="numero">Ingresa un número:</label>
  <input type="text" id="numero" name="numero"><br>
  <button type="submit">Invertir número</button>
</form>
