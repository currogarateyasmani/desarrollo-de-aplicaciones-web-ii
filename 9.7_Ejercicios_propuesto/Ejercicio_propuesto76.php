<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $base = $_POST['base'];
  $altura = $_POST['altura'];
  $area = area_rectangulo($base, $altura);
  echo "El Área del rectángulo es: $area";
}
function area_rectangulo($base, $altura) {
  $area = $base * $altura;
  return $area;
}
?>
<form method="post">
  <label for="base">Ingrese la base:</label>
  <input type="text" id="base" name="base"><br>
  <label for="altura">Ingrese la altura:</label>
  <input type="text" id="altura" name="altura"><br>
  <button type="submit">Calcular área</button>
</form>
