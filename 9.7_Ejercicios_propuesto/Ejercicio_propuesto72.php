<!DOCTYPE html>
<html>
<head>
  <title>Calcula el Promedio las notas</title>
</head>
<body>
  <h1>Calcula el Promedio de las notas</h1>
  <form method="post" action="">
    <label for="nota1">Nota 1:</label>
    <input type="number" name="nota1" id="nota1" required><br>
    <label for="nota2">Nota 2:</label>
    <input type="number" name="nota2" id="nota2" required><br>
    <label for="nota3">Nota 3:</label>
    <input type="number" name="nota3" id="nota3" required><br>
    <input type="submit" name="submit" value="Calcular Promedio final">
  </form>

  <?php
  if (isset($_POST['submit'])) {
    $nota1 = $_POST['nota1'];
    $nota2 = $_POST['nota2'];
    $nota3 = $_POST['nota3'];
    $notas = array($nota1, $nota2, $nota3);
    sort($notas);
    $promedio = ($notas[1] + $notas[2]) / 2;
    echo "<p>El promedio de las notas $nota1, $nota2 y $nota3 es: $promedio</p>";
  }
  ?>
</body>
</html>
