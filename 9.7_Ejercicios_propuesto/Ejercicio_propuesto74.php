<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $numero = $_POST['numero'];
  obtener_suma_digitos($numero, $suma_pares, $suma_impares);
  echo "La suma de los dígitos pares del número $numero es: $suma_pares<br>";
  echo "La suma de los dígitos impares del número $numero es: $suma_impares";
}
function obtener_suma_digitos($numero, &$suma_pares, &$suma_impares) {
  $suma_pares = 0;
  $suma_impares = 0;
  $digitos = str_split($numero);
  foreach ($digitos as $digito) {
    if ($digito % 2 == 0) {
      $suma_pares += $digito;
    } else {
      $suma_impares += $digito;
    }
  }
}
?>
<form method="post">
  <label for="numero">Ingresa un número:</label>
  <input type="text" id="numero" name="numero"><br>
  <button type="submit">Calcular</button>
</form>
