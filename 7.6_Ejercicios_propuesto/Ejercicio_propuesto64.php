<form method="POST">
  <label for="letra">Ingrese una letra:</label>
  <input type="text" name="letra" id="letra" maxlength="1" pattern="[A-Za-z]">
  <button type="submit">Verificar</button>
</form>

<?php
if (isset($_POST["letra"])) {
  $letra = $_POST["letra"];

  if (empty($letra)) {
    echo "Por favor, ingrese una letra.";
  } elseif (!ctype_alpha($letra)) {
    echo "El valor ingresado no es una letra del alfabeto.";
  } else {
    if (ctype_upper($letra)) {
      echo "La letra ingresada está en mayúscula.";
    } else {
      echo "La letra ingresada está en minúscula.";
    }
  }
}
?>
