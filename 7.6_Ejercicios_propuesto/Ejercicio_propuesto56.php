<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $num1 = $_POST['num1'];
    $num2 = $_POST['num2'];
    $num3 = $_POST['num3'];
    $num4 = $_POST['num4'];
    $num5 = $_POST['num5'];
    $num6 = $_POST['num6'];

    $matriz = array(
        array($num1, $num2),
        array($num3, $num4),
        array($num5, $num6)
    );

    $sumas_filas = array();
    foreach ($matriz as $fila) {
        $suma = 0;
        foreach ($fila as $valor) {
            $suma += $valor;
        }
        $sumas_filas[] = $suma;
    }


    foreach ($sumas_filas as $suma) {
        echo "La suma de la fila es: $suma<br>";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Suma de filas de una matrices de 3x2</title>
</head>
<body>
    <h1>Ingreso números de las matrices:</h1>
    <form method="POST">
        <label>Fila 1:</label>
        <input type="number" name="num1" required>
        <input type="number" name="num2" required><br>

        <label>Fila 2:</label>
        <input type="number" name="num3" required>
        <input type="number" name="num4" required><br>

        <label>Fila 3:</label>
        <input type="number" name="num5" required>
        <input type="number" name="num6" required><br>

        <button type="submit">Calcular</button>
    </form>
</body>
</html>

