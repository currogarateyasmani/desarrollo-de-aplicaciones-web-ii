<form method="post" action="Ejercicio_propuesto62.php">
  Introduzca su nombre: <input type="text" name="name">
  <input type="submit" value="Submit">
</form>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $name = $_POST["name"];
  if (empty($name)) {
    echo "Por favor, ingrese un nombre.";
  } else {
    $reversed_name = strrev($name);
    echo "Tu nombre escrito al revés es: " . $reversed_name;
  }
}
?>
