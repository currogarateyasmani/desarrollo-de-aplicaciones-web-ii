<!DOCTYPE html>
<html>
<head>
	<title>Matrices con valor K</title>
</head>
<body>
	<form method="POST">
		<label>Valor de K:</label>
		<input type="number" name="k" required>
		<br>
		<label>Ingreso números de la matrices:</label>
		<br>
		<input type="number" name="matrices[0][0]" required>
		<input type="number" name="matrices[0][1]" required>
		<input type="number" name="matrices[0][2]" required>
		<br>
		<input type="number" name="matrices[1][0]" required>
		<input type="number" name="matrices[1][1]" required>
		<input type="number" name="matrices[1][2]" required>
		<br>
		<input type="submit" value="Calcular">
	</form>
	<br>
	<?php
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$k = $_POST['k'];
		$matriz = $_POST['matrices'];

		foreach ($matriz as &$fila) {
		    foreach ($fila as &$valor) {
		        $valor *= $k;
		    }
		}
		$suma = 0;
		foreach ($matriz as $fila) {
		    foreach ($fila as $valor) {
		        $suma += $valor;
		    }
		}
		echo "<p>Matrices original:</p>";
		echo "<table>";
		foreach ($matriz as $fila) {
		    echo "<tr>";
		    foreach ($fila as $valor) {
		        echo "<td>$valor</td>";
		    }
		    echo "</tr>";
		}
		echo "</table>";

		echo "<p>Matrices multiplicada por $k:</p>";
		echo "<table>";
		foreach ($matriz as $fila) {
		    echo "<tr>";
		    foreach ($fila as $valor) {
		        echo "<td>$valor</td>";
		    }
		    echo "</tr>";
		}
		echo "</table>";

		echo "<p>La suma de los números de la matrices es: $suma</p>";
	}
	?>
</body>
</html>
