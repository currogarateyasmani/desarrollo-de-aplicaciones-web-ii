<!DOCTYPE html>
<html>
<head>
    <title>Ordenar numeros Descendente a Ascendente a</title>
</head>
<body>
    <form method="post">
        <label for="numeros">Ingrese varios números separados por espacios:</label><br>
        <input type="text" id="numeros" name="numeros"><br>
        <label for="forma">Seleccione la forma de ordenamiento:</label><br>
        <select id="forma" name="forma">
            <option value="asc">Ascendente</option>
            <option value="desc">Descendente</option>
        </select><br><br>
        <input type="submit" value="Ordenar">
    </form>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $numeros = explode(" ", $_POST["numeros"]);
            $forma = $_POST["forma"];

            if (count($numeros) >= 2) {
                if ($forma == "asc") {
                    sort($numeros);
                } else {
                    rsort($numeros);
                }

                echo "Números ordenados: " . implode(", ", $numeros);
            } else {
                echo "Debe ingresar al menos 2 números separados por espacios.";
            }
        }
    ?>
</body>
</html>
