<!DOCTYPE html>
<html>
<head>
	<title>Contar palabras repetidas en una frase</title>
</head>
<body>
	<h1>Contar palabras repetidas en una frase</h1>
	<form method="post">
		<label for="frase">Ingresa una frase:</label>
		<input type="text" id="frase" name="frase" required>
		<input type="submit" value="Contar palabras">
	</form>
	<?php
	if (isset($_POST['frase'])) {
		$frase = strtolower(trim($_POST['frase']));
		$frase = preg_replace('/[^a-zA-Z0-9áéíóúüñÁÉÍÓÚÜÑ ]/u', '', $frase);
		$palabras = explode(" ", $frase);

		$contador = array();
		foreach ($palabras as $palabra) {
			if (array_key_exists($palabra, $contador)) {
				$contador[$palabra]++;
			} else {
				$contador[$palabra] = 1;
			}
		}
		echo "<h2>Resultados:</h2>";
		if (empty($contador)) {
			echo "<p>No se encontraron palabras en la frase ingresada.</p>";
		} else {
			foreach ($contador as $palabra => $cantidad) {
				echo "<p>'$palabra' se repite $cantidad veces</p>";
			}
		}
	}
	?>
</body>
</html>
