<?php
if(isset($_POST['submit'])){ 
    $numeros = array($_POST['num1'], $_POST['num2'], $_POST['num3'], $_POST['num4']); 

    
    $mayor = max($numeros); 
    $menor = min($numeros); 

    echo "El número mayor es: " . $mayor . "<br>";
    echo "El número menor es: " . $menor;
}
?>

<form method="post">
    <label for="num1">Número 1:</label>
    <input type="number" name="num1"><br>

    <label for="num2">Número 2:</label>
    <input type="number" name="num2"><br>

    <label for="num3">Número 3:</label>
    <input type="number" name="num3"><br>

    <label for="num4">Número 4:</label>
    <input type="number" name="num4"><br>

    <input type="submit" name="submit" value="Enviar">
</form>
<style>
    form {  
        margin: 40px;
        padding: 100px;
        border: 5px solid red;
        width: 200px;
    }
    label {
        display: inline-block;
        width: 200px;
        margin-bottom: 20px;
    }
    input[type=number] {
        width: 200px;
        padding: 20px;
        margin-bottom: 10px;
    }
    input[type=submit] {
        padding: 10px;
        background-color: #4CAF50;
        color: red;
        border: none;
        cursor: pointer;
    }
    input[type=submit]:hover {
        background-color: #45a049;
    }
    .resultado {
        margin: 40px;
        padding: 20px;
        border: 1px solid black;
        width: 300px;
    }
    .resultado h3 {
        margin-top: 0;
    }
</style>
