<form method="POST">
  <label for="palabra">Ingrese una palabra:</label>
  <input type="text" name="palabra" id="palabra" required>
  <button type="submit">Comprobar</button>
</form>
<?php
if (isset($_POST["palabra"])) {
  $palabra = strtolower(trim($_POST["palabra"]));
  if (empty($palabra)) {
    echo "Por favor, ingrese una palabra.";
  } else {
    $palabra_reversa = strrev($palabra);
    if ($palabra_reversa === $palabra) {
      echo "La palabra es un palíndromo.";
    } else {
      echo "La palabra no es un palíndromo.";
    }
  }
}
?>
