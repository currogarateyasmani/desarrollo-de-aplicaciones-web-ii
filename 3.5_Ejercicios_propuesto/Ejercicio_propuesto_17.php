<!DOCTYPE html>
<html>
<head>
	<title>Calcular saldo actual</title>
</head>
<body>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<label for="saldo">Saldo anterior:</label>
		<input type="number" name="saldo" id="saldo"><br>

		<label for="movimiento">Tipo de movimiento (R/D):</label>
		<input type="text" name="movimiento" id="movimiento"><br>

		<label for="monto">Monto de la transacción:</label>
		<input type="number" name="monto" id="monto"><br>

		<input type="submit" value="Calcular">
	</form>
	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (!empty($_POST['saldo']) && !empty($_POST['movimiento']) && !empty($_POST['monto'])) {
			$saldo_anterior = intval($_POST['saldo']);
			$movimiento = strtoupper($_POST['movimiento']);
			$monto = intval($_POST['monto']);

			if ($movimiento == 'R') {
				$saldo_actual = $saldo_anterior - $monto;
				echo "Saldo actual: $saldo_actual";
			} elseif ($movimiento == 'D') {
				$saldo_actual = $saldo_anterior + $monto;
				echo "Saldo actual: $saldo_actual";
			} else {
				echo "Tipo de movimiento inválido. Ingrese 'R' o 'D'.";
			}
		} else {
			echo "Ingrese los datos solicitados.";
		}
	}
	?>
</body>
</html>
