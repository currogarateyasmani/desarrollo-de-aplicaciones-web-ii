<!DOCTYPE html>
<html>
<head>
	<title>Obtener el doble o triple de un número entero</title>
</head>
<body>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<label for="numero">Número:</label>
		<input type="text" name="numero" id="numero"><br>

		<input type="submit" value="Enviar">
	</form>
	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (!empty($_POST['numero'])) {
			$numero = intval($_POST['numero']);

			if ($numero > 0) {
				$resultado = $numero * 2;
			} elseif ($numero < 0) {
				$resultado = $numero * 3;
			} else {
				$resultado = 0;
			}

			echo "El resultado es: " . $resultado;
		} else {
			echo "Por favor ingrese un número";
		}
	}
	?>
</body>
</html>
