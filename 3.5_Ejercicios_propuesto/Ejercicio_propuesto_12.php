<!DOCTYPE html>
<html>
<body>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  Primer número: <input type="number" name="num1"><br>
  Segundo número: <input type="number" name="num2"><br>
  <input type="submit" value="Encontrar el menor">
</form>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $num1 = $_POST["num1"];
  $num2 = $_POST["num2"];
  $menor = ($num1 < $num2) ? $num1 : $num2;
  echo "El número menor es " . $menor;
}
?>
</body>
</html>
