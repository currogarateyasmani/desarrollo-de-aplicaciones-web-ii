<!DOCTYPE html>
<html>
<head>
    <title>Determinar tipo de triángulo según sus lados</title>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="lado1">Lado 1:</label>
        <input type="number" name="lado1" id="lado1"><br>
        <label for="lado2">Lado 2:</label>
    <input type="number" name="lado2" id="lado2"><br>

    <label for="lado3">Lado 3:</label>
    <input type="number" name="lado3" id="lado3"><br>

    <input type="submit" value="Comprobar">
</form>

<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $lado1 = $_POST['lado1'];
        $lado2 = $_POST['lado2'];
        $lado3 = $_POST['lado3'];

        if (esTriangulo($lado1, $lado2, $lado3)) {
            if ($lado1 == $lado2 && $lado2 == $lado3) {
                echo "El triángulo es equilátero";
            } else if ($lado1 == $lado2 || $lado1 == $lado3 || $lado2 == $lado3) {
                echo "El triángulo es isósceles";
            } else {
                echo "El triángulo es escaleno";
            }
        } else {
            echo "Las longitudes no forman un triángulo";
        }
    }

    function esTriangulo($a, $b, $c) {
        if ($a + $b > $c && $b + $c > $a && $c + $a > $b) {
            return true;
        } else {
            return false;
        }
    }
 ?>
</body>
</html>