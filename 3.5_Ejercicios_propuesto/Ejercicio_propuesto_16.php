<!DOCTYPE html>
<html>
<head>
	<title>Calcular promedio de aprobado o desaprobado</title>
</head>
<body>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<label for="nota1">Nota 1:</label>
		<input type="number" name="nota1" id="nota1"><br>

		<label for="nota2">Nota 2:</label>
		<input type="number" name="nota2" id="nota2"><br>

		<label for="nota3">Nota 3:</label>
		<input type="number" name="nota3" id="nota3"><br>

		<label for="nota4">Nota 4:</label>
		<input type="number" name="nota4" id="nota4"><br>

		<input type="submit" value="Calcular">
	</form>
	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (!empty($_POST['nota1']) && !empty($_POST['nota2']) && !empty($_POST['nota3']) && !empty($_POST['nota4'])) {
			$notas = array(intval($_POST['nota1']), intval($_POST['nota2']), intval($_POST['nota3']), intval($_POST['nota4']));

			rsort($notas);
			$promedio = ($notas[0] + $notas[1] + $notas[2]) / 3;

			if ($promedio >= 11) {
                echo "Aprobado";
            } else {
                echo "Desaprobado";
            }  
		}          
	}
	?>
</body>
</html>

