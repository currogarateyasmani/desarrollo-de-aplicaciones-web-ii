<!DOCTYPE html>
<html>
<head>
	<title>Determinar si dos números son iguales o diferentes</title>
</head>
<body>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<label for="num1">Número 1:</label>
		<input type="text" name="num1" id="num1"><br>

		<label for="num2">Número 2:</label>
		<input type="text" name="num2" id="num2"><br>

		<input type="submit" value="Enviar">
	</form>

	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (!empty($_POST['num1']) && !empty($_POST['num2'])) {
			$num1 = intval($_POST['num1']);
			$num2 = intval($_POST['num2']);

			if ($num1 == $num2) {
				echo "Los números son iguales";
			} else {
				echo "Los números son diferentes";
			}
		} else {
			echo "Por favor ingrese dos números";
		}
	}
	?>
</body>
</html>

