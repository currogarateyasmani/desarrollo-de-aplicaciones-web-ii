<!DOCTYPE html>
<html>
<head>
    <title>Determinar si tres longitudes forman un triángulo</title>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="lado1">Lado 1:</label>
        <input type="number" name="lado1" id="lado1"><br>

        <label for="lado2">Lado 2:</label>
        <input type="number" name="lado2" id="lado2"><br>

        <label for="lado3">Lado 3:</label>
        <input type="number" name="lado3" id="lado3"><br>

        <input type="submit" value="Comprobar">
    </form>
    <?php
        function esTriangulo($lado1, $lado2, $lado3) {
            if (($lado1 + $lado2 > $lado3) && ($lado1 + $lado3 > $lado2) && ($lado2 + $lado3 > $lado1)) {
                return "Sí, los lados ingresados forman un triángulo";
            } else {
                return "No, los lados ingresados no forman un triángulo";
            }
        }

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $lado1 = $_POST['lado1'];
            $lado2 = $_POST['lado2'];
            $lado3 = $_POST['lado3'];

            echo esTriangulo($lado1, $lado2, $lado3);
        }
    ?>
</body>
</html>

