<!DOCTYPE html>
<html>
<head>
	<title>Ordenar números enteros</title>
</head>
<body>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<label for="num1">Número 1:</label>
		<input type="text" name="num1" id="num1"><br>

		<label for="num2">Número 2:</label>
		<input type="text" name="num2" id="num2"><br>

		<label for="num3">Número 3:</label>
		<input type="text" name="num3" id="num3"><br>

		<input type="submit" value="Ordenar">
	</form>

	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (!empty($_POST['num1']) && !empty($_POST['num2']) && !empty($_POST['num3'])) {
			$num1 = intval($_POST['num1']);
			$num2 = intval($_POST['num2']);
			$num3 = intval($_POST['num3']);
			$ascendente = [$num1, $num2, $num3];
			sort($ascendente);
			echo "Orden ascendente: " . implode(", ", $ascendente) . "<br>";
			$descendente = [$num1, $num2, $num3];
			rsort($descendente);
			echo "Orden descendente: " . implode(", ", $descendente);
		} else {
			echo "Por favor ingrese tres números";
		}
	}
	?>
</body>
</html>
