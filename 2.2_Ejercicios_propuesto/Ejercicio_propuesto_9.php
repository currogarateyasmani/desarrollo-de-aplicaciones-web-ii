<!DOCTYPE html>
<html>
<body>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  Base: <input type="number" name="base"><br>
  Altura: <input type="number" name="altura"><br>
  <input type="submit" value="Calcular">
</form>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $base = $_POST["base"];
  $altura = $_POST["altura"];
  $area = $base * $altura;
  $perimetro = 2 * ($base + $altura);
  echo "El área del rectángulo es: " . $area . "<br>";
  echo "El perímetro del rectángulo es: " . $perimetro;
}
?>
</body>
</html>
