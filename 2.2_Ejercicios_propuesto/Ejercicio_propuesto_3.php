<!DOCTYPE html>
<html>
<body>

<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  Cantidad en milímetros: <input type="number" name="milimetros"><br>
  <input type="submit" value="Enviar">
</form>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $milimetros = $_POST["milimetros"];


  $metros = (int) ($milimetros / 1000); 
  $resto = $milimetros % 1000; 
  $decimetros = (int) ($resto / 100);
  $resto = $resto % 100;
  $centimetros = (int) ($resto / 10); 
  echo $metros . " metros, " . $decimetros . " decímetros, " . $centimetros . " centímetros y " . $milimetros . " milímetros";
}
?>

</body>
</html>
