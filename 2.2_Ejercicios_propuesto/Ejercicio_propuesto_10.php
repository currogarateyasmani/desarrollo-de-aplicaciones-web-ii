<!DOCTYPE html>
<html>
<body>
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
  Grados sexagesimales: <input type="number" name="grados"><br>
  <input type="submit" value="Convertir">
</form>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $grados = $_POST["grados"];
  $centesimales = ($grados / 90) * 100;
  echo $grados . " grados sexagesimales equivalen a " . $centesimales . " grados centesimales.";
}
?>
</body>
</html>
