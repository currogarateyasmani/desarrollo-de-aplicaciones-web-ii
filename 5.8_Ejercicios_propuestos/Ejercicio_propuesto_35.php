<!DOCTYPE html>
<html>
<head>
	<title>Determinar cantidad de dígitos 0</title>
</head>
<body>
	<form method="post">
		<label>Ingrese un número: </label>
		<input type="number" name="numero" required>
		<button type="submit">Calcular</button>
	</form>

	<?php
	if(isset($_POST['numero'])) {
		$numero = $_POST['numero'];
		$cantidad_ceros = 0;

		while($numero != 0) {
			$digito = $numero % 10;
			if($digito == 0) {
				$cantidad_ceros++;
			}
			$numero = (int) ($numero / 10);
		}

		echo "<p>El número ingresado contiene $cantidad_ceros dígitos 0</p>";
	}
	?>
</body>
</html>
