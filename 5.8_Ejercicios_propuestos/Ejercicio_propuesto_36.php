<!DOCTYPE html>
<html>
<head>
	<title>Porcentaje de números pares, impares y neutros</title>
</head>
<body>
	<form method="POST" action="">
		<label>Ingrese un número:</label>
		<input type="number" name="numero" required>
		<button type="submit">Calcular</button>
	</form>
	<?php
	if (isset($_POST['numero'])) {
		$numero = $_POST['numero'];
		$pares = 0;
		$impares = 0;
		$neutros = 0;
		for ($i = 0; $i < strlen($numero); $i++) {
			$digito = substr($numero, $i, 1);
			if ($digito == 0) {
				$neutros++;
			} elseif ($digito % 2 == 0) {
				$pares++;
			} else {
				$impares++;
			}
		}
		$total = $pares + $impares + $neutros;
		$porc_pares = ($pares / $total) * 100;
		$porc_impares = ($impares / $total) * 100;
		$porc_neutros = ($neutros / $total) * 100;
		echo "<p>Porcentaje de números pares: " . number_format($porc_pares, 2) . "%</p>";
		echo "<p>Porcentaje de números impares: " . number_format($porc_impares, 2) . "%</p>";
		echo "<p>Porcentaje de números neutros (0): " . number_format($porc_neutros, 2) . "%</p>";
	}
	?>
</body>
</html>
