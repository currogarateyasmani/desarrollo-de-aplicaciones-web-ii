<!DOCTYPE html>
<html>
<head>
	<title>Contador de números primos</title>
</head>
<body>
	<h1>Contador de números primos</h1>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
		<label for="inicio">Valor de inicio:</label>
		<input type="text" name="inicio" id="inicio"><br>

		<label for="fin">Valor final:</label>
		<input type="text" name="fin" id="fin"><br>

		<input type="submit" value="Calcular">
	</form>
	<?php
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$inicio = $_POST['inicio'];
		$fin = $_POST['fin'];
		function esPrimo($numero) {
		  if ($numero == 1) {
		    return false;
		  }
		  for ($i = 2; $i <= sqrt($numero); $i++) {
		    if ($numero % $i == 0) {
		      return false;
		    }
		  }
		  return true;
		}
		$numPrimos = 0;
		for ($i = $inicio; $i <= $fin; $i++) {
		  if (esPrimo($i)) {
		    $numPrimos++;
		  }
		}
		echo "El rango de $inicio a $fin contiene $numPrimos números primos.";
	}
	?>
</body>
</html>
