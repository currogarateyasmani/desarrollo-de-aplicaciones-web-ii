<?php
if(isset($_POST['submit'])){
    $num = $_POST['num'];
    $factorial = 1;
    for ($i = 1; $i <= $num; $i++){
        $factorial *= $i;
    }
    echo "El factorial de $num es $factorial";
}
?>
<form method="POST">
    <label>Ingrese un número:</label>
    <input type="number" name="num" required>
    <br><br>
    <button type="submit" name="submit">Calcular factorial</button>
</form>
