<!DOCTYPE html>
<html>
<head>
	<title>Cálculo del mcd por factorización simultánea</title>
</head>
<body>
	<h1>Cálculo del mcd por factorización simultánea</h1>
	<form action="" method="post">
		<label for="num1">Primer número:</label>
		<input type="text" name="num1" id="num1"><br>

		<label for="num2">Segundo número:</label>
		<input type="text" name="num2" id="num2"><br>

		<input type="submit" value="Calcular">
	</form>

	<?php
	function descomponerEnFactoresPrimos($numero) {
	  $factores = array();
	  $divisor = 2;
	  while ($numero > 1) {
	    if ($numero % $divisor == 0) {
	      array_push($factores, $divisor);
	      $numero = $numero / $divisor;
	    } else {
	      $divisor++;
	    }
	  }
	  return $factores;
	}

	if (isset($_POST['num1']) && isset($_POST['num2'])) {
		$num1 = $_POST['num1'];
		$num2 = $_POST['num2'];
		$factores1 = descomponerEnFactoresPrimos($num1);
		$factores2 = descomponerEnFactoresPrimos($num2);
		$factoresComunes = array_intersect($factores1, $factores2);
		$mcd = 1;
		foreach ($factoresComunes as $factor) {
			$mcd *= $factor;
		}
		echo "<p>El MCD de $num1 y $num2 es: $mcd</p>";
	}
	?>
</body>
</html>
