<!DOCTYPE html>
<html>
<head>
	<title>Suma y producto de los primeros N múltiplos de 3</title>
</head>
<body>
	<h1>Suma y producto de los primeros N múltiplos de 3</h1>
	<form action="" method="post">
		<label for="N">Ingrese un número N:</label>
		<input type="number" name="N" id="N" min="1" required><br>
		<input type="submit" value="Calcular">
	</form>

	<?php
	if(isset($_POST['N'])){
		$N = $_POST['N'];
		$suma = 0;
		$producto = 1;
		for ($i = 1; $i <= $N; $i++) {
		  $numero = $i * 3; 
		  $suma += $numero; 
		  $producto *= $numero; 
		}
		echo "<p>La suma de los primeros $N números naturales múltiplos de 3 es: $suma</p>";
		echo "<p>El producto de los primeros $N números naturales múltiplos de 3 es: $producto</p>";
	}
	?>
</body>
</html>
