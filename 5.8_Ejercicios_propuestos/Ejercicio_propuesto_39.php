<!DOCTYPE html>
<html>
<head>
	<title>Cálculo del MCD por el método de Euclides</title>
</head>
<body>
	<h1>Cálculo del MCD por el método de Euclides</h1>
	<form action="" method="post">
		<label for="numero1">Primer número:</label>
		<input type="text" name="numero1" id="numero1"><br>

		<label for="numero2">Segundo número:</label>
		<input type="text" name="numero2" id="numero2"><br>

		<input type="submit" value="Calcular">
	</form>

	<?php
	function euclides($a, $b) {
	  while ($b != 0) {
	    $r = $a % $b;
	    $a = $b;
	    $b = $r;
	  }
	  return $a;
	}
	if (isset($_POST['numero1']) && isset($_POST['numero2'])) {
		$numero1 = $_POST['numero1'];
		$numero2 = $_POST['numero2'];
		$mcd = euclides($numero1, $numero2);
		echo "<p>El MCD de $numero1 y $numero2 es $mcd.</p>";
	}
	?>
</body>
</html>
