<!DOCTYPE html>
<html>
<head>
	<title>Contador de números capicúa</title>
</head>
<body>
	<h1>Contador de números capicúa</h1>
	<form method="post">
		<label for="inicio">Valor de inicio:</label>
		<input type="number" name="inicio" id="inicio" required><br>

		<label for="fin">Valor final:</label>
		<input type="number" name="fin" id="fin" required><br>

		<input type="submit" name="submit" value="Calcular">
	</form>

	<?php
	function esCapicua($numero) {
	  $numInvertido = strrev($numero); 
	  if ($numero == $numInvertido) { 
	    return true;
	  }
	  return false;
	}
	if (isset($_POST['submit'])) {
		$inicio = $_POST['inicio'];
		$fin = $_POST['fin'];
		$numCapicua = 0;
		for ($i = $inicio; $i <= $fin; $i++) {
			if (esCapicua($i)) {
				$numCapicua++;
			}
		}
		echo "<p>El rango de $inicio a $fin contiene $numCapicua números capicúa.</p>";
	}
	?>
</body>
</html>

