<form method="post">
  <label for="start">Inicio:</label>
  <input type="number" name="start" id="start">
  <br>
  <label for="end">Fin:</label>
  <input type="number" name="end" id="end">
  <br>
  <button type="submit">Calcular</button>
</form>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $start = $_POST["start"];
  $end = $_POST["end"];
  $even_count = 0;
  $odd_count = 0;

  for ($i = $start; $i <= $end; $i++) {
    if ($i % 5 == 0) {
      continue;
    } else if ($i % 2 == 0) {
      $even_count++;
    } else {
      $odd_count++;
    }
  }

  echo "En el rango de $start a $end hay $even_count números pares y $odd_count números impares (sin contar múltiplos de 5).";
}
?>
