<?php
// Definimos los números a ordenar
$numeros = array(4, 2, 6, 1, 3);

// Obtenemos la forma de ordenamiento (A o D)
$forma = "A";

// Ordenamos los números según la forma indicada
if ($forma == "A") {
  sort($numeros);
} else if ($forma == "D") {
  rsort($numeros);
}

// Mostramos los números ordenados
foreach ($numeros as $numero) {
  echo $numero . " ";
}
?>

