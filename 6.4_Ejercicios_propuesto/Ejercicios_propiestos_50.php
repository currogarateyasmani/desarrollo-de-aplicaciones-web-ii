<?php
if (isset($_POST['btnCalcular'])) {
    $n = (int)$_POST['txtCifras'];
    $cantidad_capicuas = 0;
    $limite_superior = pow(10, $n) - 1;
    $limite_inferior = pow(10, $n - 1);
    for ($i = $limite_inferior; $i <= $limite_superior; $i++) {
        $str_i = strval($i);
        if ($str_i == strrev($str_i)) {
            $cantidad_capicuas++;
        }
    }
}
?>
<html>

<head>
    <title>Problema</title>
</head>

<body>
    <form method="post" action="">
        <label>Cantidad de numeros que quiere calcular:</label>
        <input type="number" name="txtCifras">
        <br><br>
        <input type="submit" name="btnCalcular" value="Calcular">
        <br><br>
        <?php if (isset($_POST['btnCalcular'])) : ?>
            <label>Números capicúas de <?= $n ?> cifras:</label>
            <input type="text" value="<?= $cantidad_capicuas ?>" readonly>
        <?php endif; ?>
    </form>
</body>
</html>
